import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import 'package:formulariobloc/src/models/producto_model.dart';
import 'package:formulariobloc/src/providers/productos_provider.dart';
import 'package:formulariobloc/utils/util.dart' as utils;


class ProductoPage extends StatefulWidget {
  @override
  _ProductoPageState createState() => _ProductoPageState();
}

class _ProductoPageState extends State<ProductoPage> {
  final formKey = GlobalKey<FormState>();
  final scaffoldKey = GlobalKey<ScaffoldState>();
  final productosProvider = new ProductosProvider();
  

  ProductoModel producto = new ProductoModel();
  bool _guardado = false;
  File foto;

/* ================================================== *
 * ==========  Widget Build   ========== *
 * ================================================== */

  @override
  Widget build(BuildContext context) {

    final ProductoModel prodData = ModalRoute.of(context).settings.arguments;
    if( prodData != null ){

      producto = prodData;

    }

  /* ================================================== *
  * ==========  Scaffold  ========== *
  * ================================================== */
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: Text('Producto'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.photo_size_select_actual), 
            onPressed: _seleccionaFoto
          ),
          IconButton(
            icon: Icon(Icons.camera_alt), 
            onPressed: _tomarFoto
          ),

        ],
      ),

      body: SingleChildScrollView(

        child: Container(
          padding: EdgeInsets.all(15.0),
          child:Form(
            key: formKey,
            child: Column(
              children: <Widget>[
                _mostrarFoto(),
                _crearNombre(),
                _crearPrecio(),
                _crearDisponible(),
                _crearBoton(context)
              ],
            ),
          )
        ),
        
      )

    );

  /* =======  End of Scaffold  ======= */
  }

/* =======  End of Widget Build   ======= */


/* ================================================== *
 * ==========  Creando input nombre  ========== *
 * ================================================== */

  Widget _crearNombre() {
    return TextFormField(
      initialValue: producto.titulo,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        labelText: 'Producto'
      ),
      onSaved: (value) => producto.titulo = value,
      validator: (value){
        if(value.length < 3){
          return 'Ingrese el nombre del producto';
        }else{
          return null;
        }
      } ,
    );
  }

/* =======  End of Creando input nombre  ======= */


/* ================================================== *
 * ==========  Creando el input de precio  ========== *
 * ================================================== */

  Widget _crearPrecio() {
    return TextFormField(
      initialValue: producto.valor.toString(),
      keyboardType: TextInputType.numberWithOptions(decimal:true),
      decoration: InputDecoration(
        labelText: 'Precio'
      ),
      onSaved: (value) => producto.valor = double.parse(value),
      validator:(value){
        
        if(utils.isNumeric(value)){
          return null;
        }
        else{
          return 'Solo debes colocar números';
        }

      }
    );
  }

/* =======  End of Creando el input de precio  ======= */


/* ================================================== *
 * ==========  Creando el boton submit  ========== *
 * ================================================== */

  Widget _crearBoton(BuildContext context) {
    return RaisedButton.icon(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0)
      ),
      color: Theme.of(context).primaryColor,
      textColor: Colors.white,
      label:Text('Guardar'),
      icon:Icon(Icons.save),      
      onPressed: ( _guardado ) ? null : _submit,
    );

  }

/* =======  End of Creando el boton submit  ======= */

/* ================================================== *
 * ==========  Creando el check  ========== *
 * ================================================== */

  Widget _crearDisponible() {

    return SwitchListTile(
      value: producto.disponible, 
      title: Text('Disponible'),
      onChanged: (value){

        setState(() {
          producto.disponible = value;
        });

      },
      activeColor: Theme.of(context).primaryColor,
    );
  }

/* =======  End of Creando el check  ======= */

/* ================================================== *
 * ==========  Submit  ========== *
 * ================================================== */

  void _submit() async {

    

    if( !formKey.currentState.validate() ) return;
    
    formKey.currentState.save();

    setState(() { _guardado = true; });

    if(foto != null){

      producto.foto = await productosProvider.subirImagen(foto);

    }

    if( producto.id != null ){
      productosProvider.modificarProducto(producto);
      mostrarSnackbar('Registro Modificado');
    }
    else{
      productosProvider.crearProducto(producto);
      mostrarSnackbar('Registro Guardado');
    }

    //setState(() { _guardado = false; });
    Navigator.pop(context);

  }

/* =======  End of Submit  ======= */

/* ================================================== *
 * ==========  Mensaje temporal  ========== *
 * ================================================== */

  void mostrarSnackbar(String mensaje){

    final mostrar  = SnackBar(

      content: Text(mensaje),
      duration: Duration(milliseconds: 1500),

    );

    scaffoldKey.currentState.showSnackBar(mostrar);

  }

/* =======  End of Mensaje temporal  ======= */

/* ================================================== *
 * ==========  Muestra la foto  ========== *
 * ================================================== */

  Widget _mostrarFoto(){
    //print(producto.foto);
    if(producto.foto != null){
      
      return FadeInImage(

        image:NetworkImage(producto.foto),
        placeholder: AssetImage('assets/jar-loading.gif'),
        height: 300.0,
        fit:BoxFit.cover

      );

    }else{

      return Image(
        image: AssetImage(foto?.path ?? 'assets/no-image.png'),
        fit: BoxFit.cover,
        height: 300.0,
      );

    }

  }

/* =======  End of Muestra la foto  ======= */

/* ================================================== *
 * ==========  Selecciona foto de la galeria  ========== *
 * ================================================== */

 _seleccionaFoto() async {
   _procesarImagen(ImageSource.gallery);
 }

/* =======  End of Selecciona foto de la galeria  ======= */


/* ================================================== *
 * ==========  Usa camara para obtener foto  ========== *
 * ================================================== */

 _tomarFoto() async{
   _procesarImagen(ImageSource.camera);
 }

/* =======  End of Usa camara para obtener foto  ======= */

_procesarImagen(ImageSource origen)async{

  foto = await ImagePicker.pickImage(
     source: origen
   );

   if(foto != null){
     producto.foto = null;
   }

   setState(() { });

}
}
