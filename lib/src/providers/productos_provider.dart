

import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;

import 'package:formulariobloc/src/models/producto_model.dart';
import 'package:mime_type/mime_type.dart';
import 'package:http_parser/http_parser.dart';

class ProductosProvider{

  final String _url = "https://flutter-varios-fb3ef.firebaseio.com";

  
  /* ================================================== *
   * ==========  Crear Producto  ========== *
   * ================================================== */

  Future<bool> crearProducto(ProductoModel producto) async{

    final url = '$_url/productos.json';
    
    final resp  =  await http.post(url, body: productoModelToJson(producto));

    final decodedData = json.decode(resp.body);

    print(decodedData);

    return true;
  } 
  
  /* =======  End of Crear Producto  ======= */

  
  /* ================================================== *
   * ==========  Editar Producto  ========== *
   * ================================================== */
  
  Future<bool> modificarProducto( ProductoModel producto ) async{

    final url = '$_url/productos/${producto.id}.json';
    
    final resp  =  await http.put(url, body: productoModelToJson(producto));

    final decodedData = json.decode(resp.body);

    print(decodedData);

    return true;
    
  } 
  
  /* =======  End of Editar Producto  ======= */
  
  /* ================================================== *
   * ==========  Mostrar Productos  ========== *
   * ================================================== */
  Future<List<ProductoModel>> cargarProductos() async {

    final url = '$_url/productos.json';

    final resp = await http.get(url);

    final Map<String,dynamic> decodedData = json.decode(resp.body);

    final List<ProductoModel> productos =  new List();

    if(decodedData == null) return [];

    decodedData.forEach((id,prod){

      final prodTemp = ProductoModel.fromJson(prod);
      prodTemp.id = id;

      productos.add( prodTemp );

    });
    //print(productos);

    return productos;

  }  
  
  /* =======  End of Mostrar Productos  ======= */

  
  /* ================================================== *
   * ==========  Borrar Producto  ========== *
   * ================================================== */
  
  Future<int> borrarProducto(String id) async {

    final url = '$_url/productos/$id.json';

    final resp = await http.delete(url);

    print(json.decode(resp.body));

    return 1;

  }
  
  /* =======  End of Borrar Producto  ======= */

  /* ================================================== *
  * ==========  Subir Imagen  ========== *
  * ================================================== */

  Future<String> subirImagen(File imagen) async{

    final url = Uri.parse('https://api.cloudinary.com/v1_1/sancezgar/image/upload?upload_preset=ipoideo2&api_key=461768735572332');
    final mimeType = mime(imagen.path).split('/'); //image/jpeg

    final imageUploadRequest = http.MultipartRequest(
      'POST', //Metodo
      url //ruta
    );

    final file = await http.MultipartFile.fromPath(
      'file', //el nombre del campo
      imagen.path, // la ruta local
      contentType: MediaType(mimeType[0],mimeType[1]) // el tipo del archivo
    );

    imageUploadRequest.files.add(file);

    final streamResponse = await imageUploadRequest.send();
    final resp = await http.Response.fromStream(streamResponse);

    if(resp.statusCode != 200 && resp.statusCode != 201){
      print('algo salio mal');
      print(resp.body);
      return null;
    }

    final respData = json.decode(resp.body);
    print( respData );

    return respData['secure_url'];

  }


  /* =======  End of Subir Imagen  ======= */




}